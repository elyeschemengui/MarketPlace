package com.example.mp.mp.Model;

import lombok.*;
import org.hibernate.type.TextType;

import javax.persistence.*;

@Data
@Entity
public class Mp_Service {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id_service;
	private String service_name;
	private Boolean status;
	private Float available_storge;
	private TextType descripition;
	private Float price;
	private TextType get_started;
	@ManyToOne
	@JoinColumn(name="id_user",foreignKey=@ForeignKey(name="id_user_fk"))
	private Mp_User user;

	public Mp_Service(String service_name, Boolean status, Float available_storge, TextType descripition, Float price,
                      TextType get_started, Mp_User user) {
		super();
		this.service_name = service_name;
		this.status = status;
		this.available_storge = available_storge;
		this.descripition = descripition;
		this.price = price;
		this.get_started = get_started;
		this.user = user;
	}
	public Mp_Service() {
		super();
	}


}
