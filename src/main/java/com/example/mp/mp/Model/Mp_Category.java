package com.example.mp.mp.Model;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
public class Mp_Category {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id_category;
	private String category_name;
	@ManyToOne
	@JoinColumn(name="id_service",foreignKey=@ForeignKey(name="id_service_fk"))
	private Mp_Service mp_Service;

	public Mp_Category() {
		super();
	}

	public Mp_Category(String category_name, Mp_Service mp_Service) {
		super();
		this.category_name = category_name;
		this.mp_Service = mp_Service;
	}


}
