package com.example.mp.mp.Model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data
@Entity
public class Mp_User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id_user;
	private String first_name;
	private String last_name;
	private String mail;
	private String password;
	private Date birthdate;
	private String country;
	private String zip;
	private int tel;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;
	public Mp_User() {
		super();
	}
	public Mp_User(String first_name, String last_name, String mail, String password, Date birthdate, String country,
                   String zip, int tel) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.mail = mail;
		this.password = password;
		this.birthdate = birthdate;
		this.country = country;
		this.zip = zip;
		this.tel = tel;
	}


}
