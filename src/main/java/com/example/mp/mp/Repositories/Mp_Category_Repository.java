package com.example.mp.mp.Repositories;

import com.example.mp.mp.Model.Mp_Category;
import org.springframework.data.repository.CrudRepository;


public interface Mp_Category_Repository extends CrudRepository<Mp_Category, Long> {

}
