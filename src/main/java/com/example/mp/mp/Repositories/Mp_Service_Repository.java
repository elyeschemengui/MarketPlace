package com.example.mp.mp.Repositories;

import com.example.mp.mp.Model.Mp_Service;
import org.springframework.data.repository.CrudRepository;


public interface Mp_Service_Repository extends CrudRepository<Mp_Service, Long> {

}
