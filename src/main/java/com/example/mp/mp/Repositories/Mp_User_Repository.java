package com.example.mp.mp.Repositories;

import com.example.mp.mp.Model.Mp_User;
import org.springframework.data.repository.CrudRepository;


public interface Mp_User_Repository extends CrudRepository<Mp_User, Long> {

}
